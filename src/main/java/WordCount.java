/**
 * Created with IntelliJ IDEA.
 * User: venuktangirala
 * Date: 6/4/13
 * Time: 4:38 PM
 * To change this template use File | Settings | File Templates.
 */// WordCount.java
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.Date;
import java.util.Formatter;
import java.util.StringTokenizer;

public class WordCount implements Tool {

    public static void main(String[] args)  throws IOException ,InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        GenericOptionsParser parser = new GenericOptionsParser(conf, args);
        args = parser.getRemainingArgs();

        Job job = new Job(conf, "wordcount");

        job.setJarByClass(WordCount.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        Formatter formatter = new Formatter();
        String outpath = "hdfs://localhost/output/"
                + formatter.format("%1$tm%1$td%1$tH%1$tM%1$tS", new Date());
        FileInputFormat.setInputPaths(job, new Path("hdfs://localhost/user/venuktangirala/data"));
        FileOutputFormat.setOutputPath(job, new Path(outpath));
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);

        System.out.println(job.waitForCompletion(true));

//        return false;
    }

    @Override
    public int run(String[] strings) throws Exception {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setConf(Configuration entries) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Configuration getConf() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}

class WordCountMapper extends
        Mapper<LongWritable, Text, Text, IntWritable> {
    private Text word = new Text();
    private final static IntWritable one = new IntWritable(1);

    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        String line = value.toString();
        StringTokenizer tokenizer = new StringTokenizer(line);
        while (tokenizer.hasMoreTokens()) {
            word.set(tokenizer.nextToken());
            context.write(word, one);
        }
    }
}

class WordCountReducer extends
        Reducer<Text, IntWritable, Text, IntWritable> {

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {


        super.setup(context);    //To change body of overridden methods use File | Settings | File Templates.
    }

    protected void reduce(Text key, Iterable<IntWritable> values,
                          Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable value : values) {
            sum += value.get();
        }

        context.write(key, new IntWritable(sum));
    }
}
